﻿namespace Login
{
	partial class Form2
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			label1 = new Label();
			textBox1 = new TextBox();
			label2 = new Label();
			label3 = new Label();
			textBox2 = new TextBox();
			button1 = new Button();
			checkBox1 = new CheckBox();
			SuspendLayout();
			// 
			// label1
			// 
			label1.AutoSize = true;
			label1.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
			label1.Location = new Point(34, 88);
			label1.Name = "label1";
			label1.Size = new Size(220, 31);
			label1.TabIndex = 0;
			label1.Text = "Enter the IP-adress:";
			// 
			// textBox1
			// 
			textBox1.Location = new Point(475, 88);
			textBox1.Name = "textBox1";
			textBox1.Size = new Size(249, 27);
			textBox1.TabIndex = 1;
			// 
			// label2
			// 
			label2.AutoSize = true;
			label2.Font = new Font("Showcard Gothic", 16.2F, FontStyle.Bold, GraphicsUnit.Point);
			label2.ForeColor = Color.Tomato;
			label2.Location = new Point(260, 9);
			label2.Name = "label2";
			label2.Size = new Size(256, 35);
			label2.TabIndex = 2;
			label2.Text = "Authorization";
			// 
			// label3
			// 
			label3.AutoSize = true;
			label3.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
			label3.Location = new Point(34, 154);
			label3.Name = "label3";
			label3.Size = new Size(366, 31);
			label3.TabIndex = 3;
			label3.Text = "Enter the password of the server:";
			// 
			// textBox2
			// 
			textBox2.Location = new Point(475, 160);
			textBox2.Name = "textBox2";
			textBox2.Size = new Size(249, 27);
			textBox2.TabIndex = 4;
			// 
			// button1
			// 
			button1.Font = new Font("Showcard Gothic", 16.2F, FontStyle.Bold, GraphicsUnit.Point);
			button1.ForeColor = Color.CornflowerBlue;
			button1.Location = new Point(225, 325);
			button1.Name = "button1";
			button1.Size = new Size(373, 82);
			button1.TabIndex = 5;
			button1.Text = "Maybe let's GO?";
			button1.UseVisualStyleBackColor = true;
			button1.Click += button1_Click;
			// 
			// checkBox1
			// 
			checkBox1.AutoSize = true;
			checkBox1.Font = new Font("Monotype Corsiva", 18F, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point);
			checkBox1.ForeColor = Color.ForestGreen;
			checkBox1.Location = new Point(176, 413);
			checkBox1.Name = "checkBox1";
			checkBox1.Size = new Size(462, 41);
			checkBox1.TabIndex = 6;
			checkBox1.Text = "Agree to the terms of confidentiality";
			checkBox1.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			AutoScaleDimensions = new SizeF(8F, 20F);
			AutoScaleMode = AutoScaleMode.Font;
			ClientSize = new Size(800, 450);
			Controls.Add(checkBox1);
			Controls.Add(button1);
			Controls.Add(textBox2);
			Controls.Add(label3);
			Controls.Add(label2);
			Controls.Add(textBox1);
			Controls.Add(label1);
			Name = "Form1";
			Text = "Form1";
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private Label label1;
		private TextBox textBox1;
		private Label label2;
		private Label label3;
		private TextBox textBox2;
		private Button button1;
		private CheckBox checkBox1;
	}
}