namespace Login
{
	public partial class Form2 : Form
	{
		public Form2()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (textBox1.Text != "127.0.0.1")
			{
				MessageBox.Show("������ ���������� IP");
				textBox1.Clear();
				return;
			}

			if (textBox2.Text != "��� ��������� ������")
			{
				MessageBox.Show("������ ���������� ������");
				textBox2.Clear();
				return;
			}

			if (!checkBox1.Checked)
			{
				MessageBox.Show("������� ����������");
				return;
			}

			this.Hide();
			Client.Form1 clientForm = new Client.Form1();
			clientForm.Show();
			this.Close();
		}
	}
}