﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
	public partial class Form2 : Form
	{
		public Form2()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (textBox1.Text != "127.0.0.1")
			{
				MessageBox.Show("Введіть правильний IP");
				textBox1.Clear();
				return;
			}

			if (textBox2.Text != "Password")
			{
				MessageBox.Show("Введіть правильний пароль");
				textBox2.Clear();
				return;
			}

			if (!checkBox1.Checked)
			{
				MessageBox.Show("Потрібно погодитися!\nP.S(Кредит на Вас ніхто не оформить 😁)");
				return;
			}

			Form1 form1 = new Form1();
			form1.Show();
			this.Hide();
		}
	}
}
